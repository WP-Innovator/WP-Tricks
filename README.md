![WP Innovator](https://scontent.fric1-1.fna.fbcdn.net/v/t1.0-9/18403092_1035507859918005_317638383374991487_n.jpg?oh=e5ba92c550ee98065190708533a3d61d&oe=5A94C0FD "WP Innovator")

# WP Innovator's Wordpress Tricks
For more info see the [wiki](https://gitlab.com/WP-Innovator/WP-Tricks/wikis/home)

## Getting Started

There are two main ways to contribute to the project. First would be just submitting a nifty little snippet by selecting the snippets section on the left hand menu.

You can also clone the repository and submit a pull-request. However please be sure to only submit after reading our [rules](https://gitlab.com/WP-Innovator/WP-Tricks/wikis/contributing) for contributing to the repo.

#### Here is a quick example of what you would do in this case
```
git clone git@gitlab.com:WP-Innovator/WP-Tricks.git
cd WP-Tricks
touch my-awesome-code
git commit -m "Contributing my-awesome-code"
git push -u origin master
```


### Bash Tricks Wiki

For more information on the project including how to contribute, acknowlegements, etc. - see our the [wiki](https://gitlab.com/waltspence/bash-tricks/wikis/home)